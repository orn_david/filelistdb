"""main.py
Development testing
"""
from filelistdb.API import API
from filelistdb.Utils.Time import time_created_float, time_float_to_string
import os

db_file = "temp.db"
if os.path.isfile(db_file):
    os.remove(db_file)
folder = "/home/pazuz/gaia/Programming/Python"

obj = API(db_file)
obj.parse_folder("python", folder)
