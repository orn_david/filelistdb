# FileListDB

This project revolves around creating a database object around filestructure. The main objective is to check if all the files in one folder are in another, but it also allows us to generate a new paradigm in checking and searching for files. 

The project was started on 17/09/21 by David Orn (cudea1@gmail.com) to solve a specific problem regarding multiple files in multiple locations. We wanted to additionally improve our monitoring of the files so we built this small standalone subsystem.

The system was written using python 3.8 and will depends on the modules listed in requirements.txt.

## Install
The package is installed using the pip methodology.