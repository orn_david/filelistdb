"""Time.os
Wrapper for fetching time values
"""

import os
from datetime import datetime


def time_created_float(file_path: str) -> float:
    """Get the time of creation as an float"""
    return os.path.getctime(file_path)


def time_modified_float(file_path: str) -> float:
    """Get the time of modifcation as a float"""
    return os.path.getmtime(file_path)


def time_float_to_string(time_stamp: float) -> str:
    """Get the string representation of a specific time evaluation"""
    return datetime.fromtimestamp(time_stamp).strftime("%Y-%m-%d %H:%M:%S")
