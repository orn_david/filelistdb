"""Files.py
Util file to handle files and file strings
"""
import pathlib
import os


def package_dir():
    """Get the top directory of the package"""
    path = pathlib.Path(__file__)
    return path.parent.absolute().parent.absolute()


def get_schema_file(filename):
    """Get a schema file by filename"""
    filename = os.path.join(package_dir(), "Schemas", filename)
    if not os.path.isfile(filename):
        raise FileNotFoundError(f"File not found {filename}")
    return filename
