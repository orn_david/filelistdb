"""API.py
File Listing Database wrapper api call function. This is all you need
to read to use the program, alongside the README.md file. The usecase
is simple, point it to a folder and the system uses a search algorithm
to find and list the metadata for all the files in the folder. Using
a pre-determined sql schema the system stored the data of interest.
This ends up creating a sql list of your files allowing for some queries
like fast search and detection of a specific file/folder.

The underlying subsystems can also be re-purposed to do a similar
pattern: search - list - store.


Current structure is a one pass stucture, i.e. you build your database once, I
now that's not how databases are supposed to work. But, in this case we are
running out of time that we can spend on this project for now.


Future steps:
    - Make system continous (i.e. no one pass build)
"""
from filelistdb.Objects.SqlObject import SqlObject
from filelistdb.Objects.SchemaParser import SchemaParser
from filelistdb.Algorithms.FileSearch import FileSearch
import os


class API(object):
    """FileListDB API call object"""
    def __init__(self, filedb) -> object:
        """Initialize the object by giving it an filename for the output file
        that will contain the database"""
        self.sql = SqlObject(filedb)

    def parse_folder(self, table_name : str, folder :str) -> None:
        """Iterate through the folder and gather the information
        about the files into a list"""
        # List all the files in the folder and build a structure
        # that in this case is unique (i.e. unique schema)
        if not os.path.isdir(folder):
            raise OSError("Directory does not exist")
        DataObject = FileSearch(folder)
        # The shcema file is selected by default
        schema_file = "fileSearch.json"
        # Create the database, V 0.1 assumes it's a one pass
        self.sql.create_table(table_name, schema_file)
        # Populate the database with the files
        self.sql.add(table_name, schema_file, DataObject.data, reduce_by=0)

    def table_described(self, schema_file: str) -> list:
        """Turn an schema file into a list"""
        schema_obj = SchemaParser(schema_file)
        print(schema_obj.create_table())

    def create_table(self, table_name: str, schema_file: str) -> []:
        """Create a table based on the a schemafile, the file will be
        stored in the created databse object under the table name"""
        self.sql.create_table(table_name, schema_file)

    def table_info(self, table_name: str) -> list:
        """Get the information about the table as a json structure"""
        return(self.sql.get_table_variables(table_name))

    def table_keys(self, schema_file: str) -> str:
        """Get a string representing the keys in the table"""
        schema_obj = SchemaParser(schema_file)
        return schema_obj.keys()

    def test_query(self, schema_file: str, table: str)-> str:
        """Test how a query is built"""
        schema_obj = SchemaParser(schema_file)
        return schema_obj.insert_query(table)

