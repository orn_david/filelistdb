"""test_UTILS.py
Testing the utils files
"""
import pytest

import os
from filelistdb.Utils.Files import package_dir
from filelistdb.Utils.Files import get_schema_file


def test_package_dir():
    assert os.path.basename(package_dir()) == "filelistdb"


def test_get_schema():
    """Test that a schema that exists is found"""
    existing_file = "fileSearch.json"
    filename = get_schema_file(existing_file)
    assert os.path.basename(filename) == existing_file


def test_fail_get_schema():
    """Test failing to get schema"""
    not_existing_filename = "notAfile"
    with pytest.raises(FileNotFoundError):
        f = get_schema_file(not_existing_filename)


