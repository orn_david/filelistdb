"""test_SQL.py
Testing the SQL implementation of the system
"""
import os
import pytest
from filelistdb.Objects.SqlObject import SqlObject
from filelistdb.Utils.Files import package_dir
from filelistdb.Utils.Files import get_schema_file

import json

filename = "temp.db"
schema_filename = "fileSearch.json"


def test_assure_file_does_not_exist():
    """We need to ensure the file doesn't exist before running the test"""
    if os.path.isfile(filename):
        os.remove(filename)


def test_create():
    """Try to create an database"""
    SqlObject(filename)


def test_create_table():
    obj = SqlObject(filename)
    obj.create_table("t1", "fileSearch.json")


def test_duplicate_create_table():
    obj = SqlObject(filename)
    with pytest.raises(RuntimeError):
        obj.create_table("t1", "fileSearch.json")


def test_cleanup():
    """Cleanup after the test"""
    os.remove(filename)
