"""test_SCHEMAPARSER.py
Testing the schema parser from Objects/SchemaParser.py
"""
import pytest
from filelistdb.Objects.SchemaParser import SchemaParser

def test_build():
    """Ensure we can create the object"""
    obj = SchemaParser("fileSearch.json")


def test_failed_build():
    """Test that it doesn't work when you use an undefined
    json file"""
    with pytest.raises(FileNotFoundError):
        obj = SchemaParser("I_dont_exists")


def test_getting_keys():
    """Test that the value returns a string of keys that
    are comma seperated"""
    obj =SchemaParser("fileSearch.json")
    keys = obj.keys()
    all_match = True
    expected = ["FileId", "Basename", "Extension", "Fullpath", "DateCreated",
            "DateModified"]

    for exp in expected:
        if exp not in keys:
            all_match = False

    assert all_match
