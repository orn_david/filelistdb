"""test_TIME.py
Testing implementation of the Utils/Time.py functionality
"""
import os
from filelistdb.Utils.Time import time_created_float, time_float_to_string
import datetime


def test_float():
    file_path = os.path.abspath(__file__)
    date = time_created_float(file_path)
    assert isinstance(date, float)

def test_str():
    file_path = os.path.abspath(__file__)
    date = time_created_float(file_path)
    date_str = time_float_to_string(date)
    assert isinstance(date_str,  str)


