"""SqlObject.py
A wrapper for an SQLITE3 function call to a database
"""
import sqlite3
from filelistdb.Objects.SchemaParser import SchemaParser

class SqlObject():
    """Handler for the sql object"""
    def __init__(self, filename):
        self.db = sqlite3.connect(filename)

    def __del__(self):
        """Deconstructor to ensure the value is closed"""
        self.db.close()

    def _fetch(self, command):
        """Wrapper for executing a generic command on the SQL stack"""
        c = self.db.cursor()
        c.execute(command)

        return c

    def _insert(self, command, *argv):
        """Input the arguments into the dataset using the command, 
        the argvar should either be a list of length 1 with the scema inputs or
        a list of length n with the schema inputs"""
        c = self.db.cursor()
        inargs = []
        # Check if there are additional arguments
        if len(argv) == 1:
            inargs = argv[0]
        # If there are additional inputs how many are they (changes the
        # function call)
        if len(inargs) > 1:
            import pdb;pdb.set_trace()
            c.executemany(command, inargs)
        else:
            c.execute(command, inargs)
        self.db.commit()
        c.close()

    def does_table_exists(self, table_name):
        """Check if a table exists in schema"""
        command = "SELECT count(name) FROM sqlite_master WHERE type='table' "\
                                        "AND name='{}'".format(table_name)
        c = self._fetch(command)

        if c.fetchone()[0] == 1:
            return True
        return False

    def create_table(self, table_name, schema_file):
        """Try to create a new table using a schema, the schema file
        should exist within filelistdb/Schema/schema_file.json as a
        json file"""
        schema_parser = SchemaParser(schema_file)
        if self.does_table_exists(table_name):
            raise RuntimeError("Table name exists")

        command = "CREATE TABLE IF NOT EXISTS " + table_name + \
                    schema_parser.sql_table_define() + ";"
        return self._insert(command)

    def add(self, table_name, schema_file, *argv, reduce_by=0):
        """Add a series of values according to the schema to the
        database"""
        schema_parser = SchemaParser(schema_file)
        command = schema_parser.insert_query(table_name, reduce_by=reduce_by)
        self._insert(command, *argv)


    def get_table_variables(self, table_name):
        """Get and parse the table variables"""
        command = "PRAGMA table_info({});".format(table_name)
        data = self._fetch(command).fetchall()
        table = []
        for indx, samples in enumerate(data):
            var_name = samples[1]
            var_type = samples[2]
            table.append((var_name, var_type))

        return table

# TODO Add more resilient comparison between the table variables and the
# original schema
