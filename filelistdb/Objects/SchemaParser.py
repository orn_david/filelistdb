"""SchemaParser.py
An object to access and parse the schema information
both to and from the sql pipeline
"""
from filelistdb.Utils.Files import get_schema_file
import json

class SchemaParser():
    """Object to parse the schema.json files and the sql database
    into a more user friendly format"""
    def __init__(self, schemaname):
        self.filename = get_schema_file(schemaname)
        with open(self.filename, 'r') as fin:
            self.data = json.load(fin)

    def sql_table_define(self):
        """Using the json schema, create a query from it"""
        out = "("
        n = len(self.data.keys())
        for indx, key in enumerate(self.data.keys()):
            subkeys = self.data[key].keys()
            out += _padding(key)
            out += _padding(self.data[key]["type"])
            if "size" in subkeys:
                out += _padding("(" + str(self.data[key]["size"]) + ")")
            out += self.data[key]["config"]
            if indx < n - 1:
                out += ","
        out += ")"

        return out

    def keys(self):
        """Return all of the keys in the schema"""
        out = "("
        for indx, key in enumerate((self.data.keys())):
            out += key
            if indx < len(self.data.keys()) - 1:
                out += ","
        out += ")"
        return out

    def qmarks(self, reduce_by=0):
        """Return insert question marks in bracket for padding into the
        input command so the query becomes correct for insertion
        reduce_by is used to reduce the number of keys, e.g. if one of the
        values in the series is autoincrement
        """
        n = len(self.data.keys()) - reduce_by
        out = "("
        for i in range(n-1):
            out += "?,"
        out += "?)"
        return out

    def insert_query(self, table_name, reduce_by=0):
        """Create an insert query with the correctly named variables and
        the correct number of question marks"""
        return "INSERT INTO "+ table_name + \
                                " VALUES" + self.qmarks(reduce_by) + ";"
# NOTE this method is not secure, in this project I was using SQLITE3 to
# refresh my sql queries a little so I didn't do a good work at ensureing the
# queries are clean. For that a framework would be better, but that obfuscates
# the queries


def _padding(instr):
    """Pad an input string with space after it and
    return the string back"""
    return instr + " "
