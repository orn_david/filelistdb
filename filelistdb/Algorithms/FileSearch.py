"""FileSearch.py
A file parsing algorithm that searches for all the files under a folder and
returns an object that can be iterated over to fill in data. This algorithm is
built around the fileSearch.json schema which will result in a database with
the following attributes
- FileId
- Basename
- Extension
- Fullpath
- DateCreated
- DateModified
- SizeBytes
"""
import os

from filelistdb.Utils.Time import time_created_float, time_modified_float


class FileSearch():
    """Iterate over a folder, list all the files and gather the attributes as
    defined above"""
    def __init__(self, topdir):
        """Create the object and iterate through the folder"""
        self.topdir = topdir
        self.data, self.n = create_list(topdir)
        self.__iterno__ = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.__iterno__ == self.n - 1:
            self.__iterno__ = 0
            raise StopIteration
        else:
            self.__iterno__ += 1
            return self.data[self.__iterno__ - 1]


def create_list(topdir):
    """Create the list with values of interest"""
    increment = 0
    dc = -1
    dm = -1
    data = []
    for (root, dirs, files) in os.walk(topdir):
        for name in files:
            [basename, extension] = os.path.splitext(name)
            fullpath = os.path.join(root, name)
            if os.path.isfile(fullpath):
                size = os.path.getsize(fullpath)
                dc = time_created_float(fullpath)
                dm = time_modified_float(fullpath)
                data.append(([increment, basename, extension,
                              fullpath, dc, dm, size]))
                increment += 1

    return data, increment



