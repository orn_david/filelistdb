import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="filelistdb",
    version="0.0.1",
    author="David Orn",
    author_email="cudea0@gmail.com",
    description="An open implementation of listing, storing and comparing files
    in different locations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/orn_david/filelistdb.git",
    packages = ['filelistdb'],
    install_requires=['numpy>=1.11.3'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: MIT Liscence",
        "Operating System :: OS Independent",
    ],
)
